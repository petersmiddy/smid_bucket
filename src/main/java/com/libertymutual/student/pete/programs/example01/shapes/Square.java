package com.libertymutual.student.pete.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;

public class Square extends Shape {

    private int length;

    public Square(int length, Color color) {
        super(color);
        this.length = length;
    }

    // provide a getArea implementation 
    @Override
    public BigDecimal getArea() {
        return new BigDecimal(length*length);
    }
}


