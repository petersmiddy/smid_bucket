package com.libertymutual.student.pete.programs.example01;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import com.libertymutual.student.pete.programs.example01.shapes.Circle;
import com.libertymutual.student.pete.programs.example01.shapes.Square;
import com.libertymutual.student.pete.programs.example01.shapes.Shape;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private final static Logger logger = LogManager.getLogger(Application.class);


    private static List<Shape> getShapes() {

        Circle circle = null;

        int radius = 10;
        try {
             circle = new Circle(radius, Color.PINK);
        } catch (Exception exception ) {
            logger.error("Bad thing has happened: " + exception.getMessage());
        }
        int length = 100;
        Square square = new Square(length, Color.RED);
        List<Shape> shapes = new ArrayList<Shape>();

        shapes.add(circle);
        shapes.add(square);

        return shapes;

    }

    public static void main(String[] args) {

        List<Shape> shapes = getShapes();

        for (Shape shape: shapes) {
            System.out.println(shape.getArea());
            System.out.println(shape.getColor());
        }

       System.exit(0);

    }
}



