package com.libertymutual.student.pete.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;

public class Circle extends Shape {

    private int radius;

    /**
     *
     *
     * @param radius Radius can't exceed 100
     * @param color
     */
    public Circle(int radius, Color color) {
        super(color);
        if (radius > 100) {
            throw new IllegalArgumentException(radius + " must be less than 100");
        }
        this.radius = radius;
    }

    /**
     *
     * @return
     */
    @Override
    public BigDecimal getArea() {
        double pi = 3.14;
        double area = pi * radius * radius;
        return new BigDecimal(area);
    }
}


